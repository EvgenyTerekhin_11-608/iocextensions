namespace IOCContainerExtensions
{
    public interface IHandler<TIn>
    {
        void Handle(TIn input);
    }
}