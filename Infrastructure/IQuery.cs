namespace IOCContainerExtensions
{
    public interface IQuery<TIn, TOut>
    {
        TOut Handle(TIn input);
    }
    
    public interface IQuery<TOut>
    {
        TOut Handle();
    }
}