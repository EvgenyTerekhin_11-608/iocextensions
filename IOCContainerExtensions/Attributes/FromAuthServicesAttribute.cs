using System;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.Extensions.DependencyInjection;

namespace IOCContainerExtensions.Attributes
{
    public class FromAuthServicesAttribute : ModelBinderAttribute
    {
        public FromAuthServicesAttribute()
        {
            BinderType = typeof(FromAuthModelBinder);
        }
    }

    public class FromAuthModelBinder : IModelBinder
    {
        private IServiceProvider _serviceProvider { get; set; }

        public FromAuthModelBinder(IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;
        }

        public Task BindModelAsync(ModelBindingContext bindingContext)
        {
            //todo return right exception
            var role = _serviceProvider.GetService<IQuery<RoleInfo>>().Handle()?.RoleType ?? throw new Exception("");

            var services = _serviceProvider.GetServices(bindingContext.ModelType);
            object service = null;
            try
            {
                service = services.SingleOrDefault(x =>
                    x.GetType().GetCustomAttribute<ServiceFor>(false).RolesType.Contains(role));
            }
            catch (Exception e)
            {
                throw new Exception(
                    $"The role must be registered only on one instance of the type implementing{bindingContext.ModelType}");
                throw;
            }

            if (service == null)
            {
                bindingContext.Result = ModelBindingResult.Failed();
                return Task.CompletedTask;
            }

            bindingContext.Result = ModelBindingResult.Success(service);
            return Task.CompletedTask;
        }
    }
}