﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using CodeGenerator;
using CodeGenerator.InterfaceGenerator;
using IOCContainerExtensions.Controllers;
using IOCContainerExtensions.Models;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.DataProtection;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using RoslynExamples;

namespace IOCContainerExtensions
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);
            services.AddDbContext<DbContext, AuthDbContext>();
            services.AddDbContext<AuthDbContext>();
            services.AddAuthentication(CookieAuthenticationDefaults.AuthenticationScheme).AddCookie();
            services.AddScoped<IQuery<RoleInfo>, GetRole>();
            services.IQueryableRegister();
            services.CodeGeneratorRegister();

            //writeInfoRegistered
            services.AddScoped<IQuery<IEnumerable<Exam>, string>, TeacherWriteInfo>();
            services.AddScoped<IQuery<IEnumerable<Exam>, string>, RectorWriteInfo>();
            //
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env, DbContext dbContext,
            IServiceProvider serviceProvider)
        {
            Seed(dbContext);
//            serviceProvider.GetService<RuleForAttributeAnalyzer>().Analyze();

            app.UseAuthentication();
            app.UseMvc(opt => { opt.MapRoute("default", "{controller}/{action}/{id?}"); });
        }


        private void Seed(DbContext dbContext)
        {
            var pupil = new User {Name = "Evgeny", Email = "eter55@yandex.ru", Password = "pass"};
            var rector = new User {Name = "Suchka", Email = "sucha@ebanaya.ru", Password = "pass"};
            var teacher = new User {Name = "Sosu", Email = "sosu@hui.ru", Password = "pass"};
            var pupilRole = new Role {Name = "Pupil", Users = new List<User> {pupil}};
            var rectorRole = new Role {Name = "Rector", Users = new List<User> {rector}};
            var teacherRole = new Role {Name = "Teacher", Users = new List<User> {teacher}};
            var exams = Enumerable.Range(1, 100).Select(x => new Exam()
            {
                Name = x.ToString()
            });
            dbContext.AddRange(pupil, rector, teacher);
            dbContext.AddRange(pupilRole, rectorRole, teacherRole);
            dbContext.AddRange(exams);
            dbContext.SaveChanges();
        }
    }
}