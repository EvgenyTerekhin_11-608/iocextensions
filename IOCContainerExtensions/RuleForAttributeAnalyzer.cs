using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using CodeGenerator.InterfaceGenerator;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Internal;

namespace IOCContainerExtensions
{
    public class RuleForAttributeAnalyzer
    {
        private readonly AccessRuleSetGenerator _accessRuleSetGenerator;
        private readonly RoleInterfaceGenerator _roleInterfaceGenerator;
        private readonly IQuery<ReWriteInfo, CompilationUnitSyntax> _interfaceReplacer;
        private readonly IQuery<AccessRuleReWriteInfo, CompilationUnitSyntax> _accessSetReplacer;
        private readonly InterfaceGenerator _interfaceGenerator;
        public const string RoslynExamplePath = "../RoslynExamples";
        public const string RoslynExamplePathToAccessSetDirectory = RoslynExamplePath + "/" + "AccessSets";

        public const string RoslynExamplePathToAccessInterfacesSetDirectory =
            RoslynExamplePath + "/" + "AccessInterfaces";

        public RuleForAttributeAnalyzer(
            AccessRuleSetGenerator accessRuleSetGenerator,
            RoleInterfaceGenerator roleInterfaceGenerator,
            IQuery<ReWriteInfo, CompilationUnitSyntax> interfaceReplacer,
            IQuery<AccessRuleReWriteInfo, CompilationUnitSyntax> accessSetReplacer)
        {
            _accessRuleSetGenerator = accessRuleSetGenerator;
            _roleInterfaceGenerator = roleInterfaceGenerator;
            _interfaceReplacer = interfaceReplacer;
            _accessSetReplacer = accessSetReplacer;
        }

        //todo refactor
        public void Analyze()
        {
            var sets = AppDomain.CurrentDomain.GetAssemblies().SelectMany(x => x.GetTypes())
                .Single(x => x.BaseType == typeof(DbContext))
                .GetProperties()
                .Where(x => x.PropertyType.IsGenericType &&
                            x.PropertyType.GetGenericTypeDefinition() == typeof(DbSet<>))
                .ToList();

            var setsInfo = sets.Select(x => new
                {
                    setName = x.Name,
                    setType = x.PropertyType.GenericTypeArguments.Single().Name,
                    roleNames = x.GetCustomAttributes().OfType<AccessRuleFor>().SingleOrDefault()
                        ?.RolesType
                        .Select(xx => $"{xx}")
                })
                .Where(x => x.roleNames != null)
                .ToList();


            var interfaces = _interfaceReplacer.Handle(new ReWriteInfo
            {
                PathToExample = $"{RoslynExamplePathToAccessInterfacesSetDirectory}/Example.cs",
                InterfaceDeclarationSyntaxs = _roleInterfaceGenerator.InterfaceGenerate()
            });

            File.WriteAllText($"{RoslynExamplePathToAccessInterfacesSetDirectory}/Interfaces.cs",
                interfaces.ToString());


            var classDeclarations = setsInfo
                .Select(setInfo => _accessRuleSetGenerator.Generate(
                    new InputAccessSetGenerateInfo
                    {
                        _dbSetSetName = setInfo.setName, _dbSetTypeName = setInfo.setType,
                        _accessRoles = setInfo.roleNames
                    }))
                .Aggregate(new List<ClassDeclarationSyntax>().AsEnumerable(), (a, c) => a.Concat(new[] {c}));


            var AC = _accessSetReplacer.Handle(new AccessRuleReWriteInfo
            {
                PathToExample = $"{RoslynExamplePathToAccessSetDirectory}/Example.cs",
                Classes = classDeclarations
            });
            File.WriteAllText($"{RoslynExamplePathToAccessSetDirectory}/AccessSet.cs", AC.ToString());
        }
    }
}