using System;
using System.Collections.Generic;
using IOCContainerExtensions.Models;
using RoslynExamples;

namespace IOCContainerExtensions
{
    public interface IHasRolesInfo
    {
        RoleType[] RolesType { get; set; }
    }

    public interface IHasRoleInfo
    {
        RoleType RoleType { get; set; }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="roles"> example: "User,SuperUser,Administrator"</param>
    [AttributeUsage(AttributeTargets.Property)]
    public class AccessRuleFor : Attribute, IHasRolesInfo
    {
        public AccessRuleFor(params RoleType[] roleType)
        {
            RolesType = roleType;
        }

        public RoleType[] RolesType { get; set; }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="roles"> example: "User,SuperUser,Administrator"</param>
    public class ServiceFor : Attribute, IHasRolesInfo
    {
        public RoleType[] RolesType { get; set; }

        public ServiceFor(params RoleType[] rolesType)
        {
            RolesType = rolesType;
        }
    }
}