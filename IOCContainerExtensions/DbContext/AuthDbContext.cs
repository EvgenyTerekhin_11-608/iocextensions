using System.Collections.Generic;
using System.Linq;
using IOCContainerExtensions.Models;
using Microsoft.EntityFrameworkCore;
using RoslynExamples;

namespace IOCContainerExtensions
{
    public class AuthDbContext : DbContext
    {
        public AuthDbContext()
        {
        }

        [AccessRuleFor(RoleType.Pupil)]
        public DbSet<Role> Roles { get; set; }

        public DbSet<User> Users { get; set; }

        [AccessRuleFor(RoleType.Pupil, RoleType.Rector, RoleType.Teacher)]
        public DbSet<Exam> Exams { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Pupil>().HasKey(x => x.Email);
            modelBuilder.Entity<Pupil>().ToTable($"{nameof(Pupil)}s");

            modelBuilder.Entity<Rector>().HasKey(x => x.Email);
            modelBuilder.Entity<Rector>().ToTable($"{nameof(Rector)}s");

            modelBuilder.Entity<Teacher>().HasKey(x => x.Email);
            modelBuilder.Entity<Teacher>().ToTable($"{nameof(Teacher)}s");
            base.OnModelCreating(modelBuilder);
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(
                "Server=.;Initial Catalog=Bot;Persist Security Info=False;Integrated Security=True;MultipleActiveResultSets=False;Connection Timeout=30;");
            base.OnConfiguring(optionsBuilder);
        }
    }

  
}