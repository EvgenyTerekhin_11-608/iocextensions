using System.Collections.Generic;
using System.Linq;
using IOCContainerExtensions.Attributes;
using IOCContainerExtensions.Models;
using Microsoft.AspNetCore.Mvc;
using RoslynExamples;

namespace IOCContainerExtensions.Controllers
{
    [Route("api/[controller]/[action]")]
    public class ExamsController : Controller
    {
        public ActionResult<IEnumerable<Exam>> Get([FromServices] IQueryable<Exam> exams) => exams.ToList();

        public ActionResult<string> WriteExamInfo(
            [FromAuthServices] IQuery<IEnumerable<Exam>, string> examHandler,
            [FromServices] IQueryable<Exam> exams) => examHandler.Handle(exams);
    }

    [Route("api/[controller]/[action]")]
    public class RolesController : Controller
    {
        public ActionResult<IEnumerable<Role>> Get([FromServices] IQueryable<Role> roles) => roles.ToList();
    }

    [Route("api/[controller]/[action]")]
    public class UsersController : Controller
    {
        public ActionResult<IEnumerable<User>> Get([FromServices] IQueryable<User> users) => users.ToList();
    }

    [ServiceFor(RoleType.Teacher,RoleType.Pupil)]
    public class TeacherWriteInfo : IQuery<IEnumerable<Exam>, string>
    {
        public string Handle(IEnumerable<Exam> input) =>
            $"Info for Teacher : {string.Join(',', input.Select(x => x.Name))}";
    }

   

    [ServiceFor(RoleType.Rector)]
    public class RectorWriteInfo : IQuery<IEnumerable<Exam>, string>
    {
        public string Handle(IEnumerable<Exam> input) =>
            $"Info for Rector : {string.Join(',', input.Select(x => x.Name))}";
    }
}