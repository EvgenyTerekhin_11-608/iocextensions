using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Security.Cryptography;
using CodeGenerator;
using CodeGenerator.InterfaceGenerator;
using Microsoft.AspNetCore.Http;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Internal;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using RoslynExamples;
using RoslynExamples.AccessSets;

namespace IOCContainerExtensions
{
    public static class IServiceCollectionExtensions
    {
        public static void IQueryableRegister(this IServiceCollection serviceCollection)
        {
            //hack for dowload assembly
            new AccessSet();
            new RolesAccessSet();
            var types = AppDomain.CurrentDomain.GetAssemblies()
                .SelectMany(x => x.GetTypes());
            var dbContext = types.Single(x => x.BaseType == typeof(DbContext));


            var setTypes = dbContext.GetProperties()
                .Where(x => x.PropertyType.IsGenericType &&
                            x.PropertyType.GetGenericTypeDefinition() == typeof(DbSet<>))
                .Select(x =>
                    new
                    {
                        queryable = x.PropertyType.GetInterfaces().Single(xx =>
                            xx.IsGenericType && xx.GetGenericTypeDefinition() == typeof(IQueryable<>)),
                        accessRoles = x.GetCustomAttribute<AccessRuleFor>(false)?.RolesType.Select(xx => $"{xx}"),
                        dbSetName = x.Name,
                        generic = x.PropertyType.GenericTypeArguments.Single()
                    })
                .Where(x => x.accessRoles != null)
                .ToList();

            foreach (var setType in setTypes)
            {
                serviceCollection.AddScoped(setType.queryable, x =>
                {
                    var _context = x.GetService<DbContext>();
                    var queryable = _context.GetType().GetMethod(nameof(DbContext.Set))
                        .MakeGenericMethod(setType.generic)
                        .Invoke(_context, new object[] { });


                    var role = x.GetService<IQuery<RoleInfo>>().Handle();
                    if (role == null) return queryable;

                    var set = types.FirstOrDefault(xx => xx.Name == $"{setType.dbSetName}AccessSet");
                    if (set == null) return queryable;

                    var accessSetInterfaces = set.GetInterfaces()
                        .Where(xx => setType.accessRoles.Any(xxx => xx.Name.Contains(xxx)));

                    var interfaceHandleMethod = accessSetInterfaces
                        .Single(xx => xx.Name.Contains(role.RoleType.ToString()))
                        .GetMethod("Handle");

                    var result = interfaceHandleMethod.Invoke(Activator.CreateInstance(set), new[] {queryable});
                    return result;
                });
            }
        }

        public static void CodeGeneratorRegister(this IServiceCollection sc)
        {
            sc.AddSingleton(typeof(ISeparatedListGenerator<,>), typeof(SeparatedSyntaxListGenerator<,>));
            sc.AddSingleton(typeof(IGeneratorByNames<,>), typeof(GeneratorByNames<,>));
            sc.AddSingleton(typeof(IOpenGeneric2QuerySyntaxTree<>), typeof(GetStringNode<>));
            sc.AddSingleton<IQuery<ReWriteInfoBase, NamespaceUsingsInfo>, GetNamespacesAndUsings>();

            //enum
            sc.AddSingleton<IGeneratorByName<string, EnumMemberDeclarationSyntax>, EnumGeneratorByName>();
            sc.AddSingleton<IGenerator<EnumGenerateInfo, EnumDeclarationSyntax>, EnumGenerator>();
            sc.AddScoped<RoleEnumGenerator>();

            //interface
            sc.AddSingleton<IGenerator<InterfaceGenerationInfo, InterfaceDeclarationSyntax>, InterfaceGenerator>();
            sc.AddScoped<RoleInterfaceGenerator>();
            sc.AddSingleton<IQuery<ReWriteInfo, CompilationUnitSyntax>, ReWriteInterfaceExample>();

            //access sets
            sc.AddSingleton<IGenerator<AccessRuleSetGenerateInfo, ClassDeclarationSyntax>, ClassGenerator>();
            sc.AddSingleton<IGeneratorByName<MemberDeclarationGenerateInfo, MemberDeclarationSyntax>,
                MemberDeclarationSyntaxByNameGenerator>();
            sc.AddSingleton<IGeneratorByName<MemberDeclarationGenerateInfo, BaseTypeSyntax>, BaseTypeSyntaxGenerator>();
            sc.AddSingleton<AccessRuleSetGenerator>();
            sc.AddSingleton<IQuery<AccessRuleReWriteInfo, CompilationUnitSyntax>, AccessRuleSetReWriter>();
            sc.AddScoped<RuleForAttributeAnalyzer>();
        }
    }
}