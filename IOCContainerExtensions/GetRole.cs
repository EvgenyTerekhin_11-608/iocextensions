using System;
using Microsoft.AspNetCore.Http;
using RoslynExamples;

namespace IOCContainerExtensions
{
    public class RoleInfo
    {
        public RoleType RoleType { get; set; }
    }

    public class GetRole : IQuery<RoleInfo>
    {
        private IHttpContextAccessor _accessor { get; set; }

        public GetRole(IHttpContextAccessor accessor)
        {
            _accessor = accessor;
        }

        public RoleInfo Handle()
        {
            var role = _accessor?.HttpContext?.User?.FindFirst(x => x.Type.Contains("role"))?.Value;
            return role == null ? null : new RoleInfo {RoleType = Enum.Parse<RoleType>(role)};
        }
    }
}