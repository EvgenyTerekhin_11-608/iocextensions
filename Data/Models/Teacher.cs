namespace IOCContainerExtensions.Models
{
    public class Teacher : UserInfo
    {
        public int ExperienceYears { get; set; }
    }
}