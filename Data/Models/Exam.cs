namespace IOCContainerExtensions.Models
{
    public class Exam
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string GroupName { get; set; }
    }
}