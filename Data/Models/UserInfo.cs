namespace IOCContainerExtensions.Models
{
    public class UserInfo
    {
        public string Email
        {
            get => User.Email;
            set { }
        }

        public User User { get; set; }
        public int UserId { get; set; }
    }
}