﻿using System;

namespace RoslynExamples
{
    public enum RoleType
    {
        Pupil,
        Rector,
        Teacher
    }
}