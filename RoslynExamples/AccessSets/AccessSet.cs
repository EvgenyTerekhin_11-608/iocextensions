using System.Linq;
using AccessInterfaces;
using IOCContainerExtensions.Models;

namespace RoslynExamples.AccessSets
{
    public class RolesAccessSet : IPupilAccessRule<Role>
    {
        IQueryable<Role> IPupilAccessRule<Role>.Handle(IQueryable<Role> query)
        {
            return query;
        }
    }

    public class ExamsAccessSet : IPupilAccessRule<Exam>, IRectorAccessRule<Exam>, ITeacherAccessRule<Exam>
    {
        IQueryable<Exam> IPupilAccessRule<Exam>.Handle(IQueryable<Exam> query)
        {
            return query;
        }

        IQueryable<Exam> IRectorAccessRule<Exam>.Handle(IQueryable<Exam> query)
        {
            return query;
        }

        IQueryable<Exam> ITeacherAccessRule<Exam>.Handle(IQueryable<Exam> query)
        {
            return query;
        }
    }
}