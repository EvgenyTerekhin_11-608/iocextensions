using System.Linq;

namespace AccessInterfaces
{
    public interface IPupilAccessRule<T>
    {
        IQueryable<T> Handle(IQueryable<T> query);
    }

    public interface IRectorAccessRule<T>
    {
        IQueryable<T> Handle(IQueryable<T> query);
    }

    public interface ITeacherAccessRule<T>
    {
        IQueryable<T> Handle(IQueryable<T> query);
    }
}