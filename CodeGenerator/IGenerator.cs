namespace CodeGenerator
{
    public interface IGenerator<T, R>
    {
        R Generate(T input);
    }
}