using System.Collections.Generic;
using Microsoft.CodeAnalysis;

namespace CodeGenerator
{
    public interface ISeparatedListGenerator<T,R> where R : SyntaxNode
    {
        SeparatedSyntaxList<R> Generate(IEnumerable<T> names);
    }
    
    

   
}