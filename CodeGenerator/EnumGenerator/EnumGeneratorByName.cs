using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.CSharp.Syntax;

namespace CodeGenerator
{
    public class EnumGeneratorByName : IGeneratorByName<string,EnumMemberDeclarationSyntax>
    {
        public EnumMemberDeclarationSyntax Generate(string name) => SyntaxFactory.EnumMemberDeclaration(SyntaxFactory.Identifier(name));
    }
}