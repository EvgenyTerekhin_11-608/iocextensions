using System.Linq;
using IOCContainerExtensions.Models;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.CSharp.Syntax;

namespace CodeGenerator
{
    public class RoleEnumGenerator
    {
        private readonly IQueryable<Role> _roles;
        private readonly IGenerator<EnumGenerateInfo, EnumDeclarationSyntax> _enumGenerator;

        public RoleEnumGenerator(
            IQueryable<Role> roles,
            IGenerator<EnumGenerateInfo, EnumDeclarationSyntax> enumGenerator)
        {
            _roles = roles;
            _enumGenerator = enumGenerator;
        }

        public EnumDeclarationSyntax EnumGeneration()
        {
            var input = new EnumGenerateInfo
            {
                Name = "Roles",
                Modifiers = SyntaxFactory.TokenList(SyntaxFactory.Token(SyntaxKind.PublicKeyword)),
                ItemNames = _roles.Select(x => x.Name).Distinct().ToList()
            };
            var result = _enumGenerator.Generate(input).NormalizeWhitespace();
            return result;
        }
    }
}