using System.Collections.Generic;

namespace CodeGenerator
{
    public class EnumGenerateInfo : BaseGenerateInfo
    {
        public IEnumerable<string> ItemNames { get; set; }
    }
}