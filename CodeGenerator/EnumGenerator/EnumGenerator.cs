using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.CSharp.Syntax;

namespace CodeGenerator
{
    public class EnumGenerator : IGenerator<EnumGenerateInfo, EnumDeclarationSyntax>
    {
        private ISeparatedListGenerator<string, EnumMemberDeclarationSyntax> SeparatedListGenerator { get; }

        public EnumGenerator(ISeparatedListGenerator<string, EnumMemberDeclarationSyntax> separatedListGenerator)
        {
            SeparatedListGenerator = separatedListGenerator;
        }

        public EnumDeclarationSyntax Generate(EnumGenerateInfo input)
        {
            var declarationSyntax = SeparatedListGenerator.Generate(input.ItemNames);
            var result = SyntaxFactory.EnumDeclaration(input.Name).WithModifiers(input.Modifiers)
                .WithMembers(declarationSyntax);
            return result;
        }
    }
}