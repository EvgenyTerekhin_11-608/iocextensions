using System.Collections.Generic;
using System.IO;
using System.Net;
using CodeGenerator.InterfaceGenerator;
using IOCContainerExtensions;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.CSharp.Syntax;

namespace CodeGenerator
{
    public class NamespaceUsingsInfo
    {
        public IEnumerable<UsingDirectiveSyntax> Usings { get; set; }
        public IEnumerable<NamespaceDeclarationSyntax> Namespaces { get; set; }
    }

    public class GetNamespacesAndUsings : IQuery<ReWriteInfoBase, NamespaceUsingsInfo>
    {
        private IOpenGeneric2QuerySyntaxTree<UsingDirectiveSyntax> GetUsingDeclarationSyntax { get; }
        private IOpenGeneric2QuerySyntaxTree<NamespaceDeclarationSyntax> GetNamespaceDeclarationSyntax { get; }

        public GetNamespacesAndUsings(IOpenGeneric2QuerySyntaxTree<UsingDirectiveSyntax> usingDeclarationSyntax,
            IOpenGeneric2QuerySyntaxTree<NamespaceDeclarationSyntax> namespaceDeclarationSyntax)
        {
            GetUsingDeclarationSyntax = usingDeclarationSyntax;
            GetNamespaceDeclarationSyntax = namespaceDeclarationSyntax;
        }

        public NamespaceUsingsInfo Handle(ReWriteInfoBase input)
        {
            var exampleSyntaxTree = CSharpSyntaxTree.ParseText(File.ReadAllText(input.PathToExample));
            return new NamespaceUsingsInfo
            {
                Usings = GetUsingDeclarationSyntax.Handle(exampleSyntaxTree),
                Namespaces = GetNamespaceDeclarationSyntax.Handle(exampleSyntaxTree)
            };
        }
    }
}