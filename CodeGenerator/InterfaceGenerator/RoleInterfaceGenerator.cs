using System.Collections.Generic;
using System.Linq;
using IOCContainerExtensions.Models;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using static Microsoft.CodeAnalysis.CSharp.SyntaxFactory;

namespace CodeGenerator.InterfaceGenerator
{
    public class RoleInterfaceGenerator
    {
        private readonly IGenerator<InterfaceGenerationInfo, InterfaceDeclarationSyntax> _interfaceGenerator;
        private IQueryable<Role> _roles { get; }

        public RoleInterfaceGenerator(IQueryable<Role> roles,
            IGenerator<InterfaceGenerationInfo, InterfaceDeclarationSyntax> interfaceGenerator)
        {
            _interfaceGenerator = interfaceGenerator;
            _roles = roles;
        }

        public IEnumerable<InterfaceDeclarationSyntax> InterfaceGenerate()
        {
            return _roles.Select(x => x.Name).Distinct().ToList()
                .Select(x => _interfaceGenerator.Generate(new InterfaceGenerationInfo
                {
                    Name = $"I{x}AccessRule",
                    Modifiers = TokenList(Token(SyntaxKind.PublicKeyword)),
                    TypeParameterList = TypeParameterList(SingletonSeparatedList(TypeParameter(Identifier("T")))),
                    MethodDeclarations = SingletonList<MemberDeclarationSyntax>(MethodDeclaration(
                            GenericName(Identifier("IQueryable"))
                                .WithTypeArgumentList(TypeArgumentList(SingletonSeparatedList<TypeSyntax>(
                                    IdentifierName("T")))), Identifier("Handle"))
                        .WithParameterList(ParameterList(SingletonSeparatedList(Parameter(Identifier("query"))
                            .WithType(GenericName(Identifier("IQueryable")).WithTypeArgumentList(
                                TypeArgumentList(SingletonSeparatedList<TypeSyntax>(
                                    IdentifierName("T"))))))))
                        .WithSemicolonToken(Token(SyntaxKind.SemicolonToken)))
                }).NormalizeWhitespace());
        }
    }
}