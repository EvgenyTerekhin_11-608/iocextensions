using System.Linq;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.CSharp.Syntax;

namespace CodeGenerator.InterfaceGenerator
{
    public class InterfaceGenerator : IGenerator<InterfaceGenerationInfo, InterfaceDeclarationSyntax>
    {
        public InterfaceDeclarationSyntax Generate(InterfaceGenerationInfo input)
            => SyntaxFactory.InterfaceDeclaration(input.Name).AddModifiers(input.Modifiers.ToArray())
                .WithTypeParameterList(input.TypeParameterList).WithMembers(input.MethodDeclarations);
    }
}