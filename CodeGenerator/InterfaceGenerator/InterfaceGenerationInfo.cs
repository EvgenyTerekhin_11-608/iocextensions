using System.Collections.Generic;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using static Microsoft.CodeAnalysis.CSharp.SyntaxFactory;

namespace CodeGenerator.InterfaceGenerator
{
    public class InterfaceGenerationInfo : BaseGenerateInfo
    {
        public TypeParameterListSyntax TypeParameterList { get; set; }
        public SyntaxList<MemberDeclarationSyntax> MethodDeclarations { get; set; }
    }
}