using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using IOCContainerExtensions;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using static Microsoft.CodeAnalysis.CSharp.SyntaxFactory;

namespace CodeGenerator.InterfaceGenerator
{
    public class ReWriteInfo : ReWriteInfoBase
    {
        public IEnumerable<InterfaceDeclarationSyntax> InterfaceDeclarationSyntaxs { get; set; }
    }

    public class ReWriteInterfaceExample : IQuery<ReWriteInfo, CompilationUnitSyntax>
    {
        public IOpenGeneric2QuerySyntaxTree<UsingDirectiveSyntax> GetUsingDeclarationSyntax { get; }
        public IOpenGeneric2QuerySyntaxTree<NamespaceDeclarationSyntax> GetNamespaceDeclarationSyntax { get; }

        public ReWriteInterfaceExample(IOpenGeneric2QuerySyntaxTree<UsingDirectiveSyntax> getUsingDeclarationSyntax,
            IOpenGeneric2QuerySyntaxTree<NamespaceDeclarationSyntax> getNamespaceDeclarationSyntax)
        {
            GetUsingDeclarationSyntax = getUsingDeclarationSyntax;
            GetNamespaceDeclarationSyntax = getNamespaceDeclarationSyntax;
        }

        public CompilationUnitSyntax Handle(ReWriteInfo input)
        {
            var syntaxTree = CSharpSyntaxTree.ParseText(File.ReadAllText(input.PathToExample));
            var usings = GetUsingDeclarationSyntax.Handle(syntaxTree).ToArray();
            var @namespace = GetNamespaceDeclarationSyntax.Handle(syntaxTree).ToArray();
            var @namespaceAndInterfaces = @namespace.First()
                .WithMembers(new SyntaxList<MemberDeclarationSyntax>(input.InterfaceDeclarationSyntaxs));
            return CompilationUnit().AddUsings(usings).AddMembers(namespaceAndInterfaces);
        }
    }
}