using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using static Microsoft.CodeAnalysis.CSharp.SyntaxFactory;

namespace CodeGenerator.InterfaceGenerator
{
    public class MemberDeclarationGenerateInfo
    {
        public string GenericTypeName { get; set; }
        public string InterfaceName { get; set; }
    }

    public class
        MemberDeclarationSyntaxByNameGenerator : IGeneratorByName<MemberDeclarationGenerateInfo, MemberDeclarationSyntax>
    {
        public MemberDeclarationSyntax Generate(MemberDeclarationGenerateInfo input) => MethodDeclaration(
                GenericName(
                        Identifier("IQueryable"))
                    .WithTypeArgumentList(
                        TypeArgumentList(
                            SingletonSeparatedList<TypeSyntax>(
                                IdentifierName(input.GenericTypeName)))),
                Identifier("Handle"))
            .WithExplicitInterfaceSpecifier(
                ExplicitInterfaceSpecifier(
                    GenericName(
                            Identifier(input.InterfaceName))
                        .WithTypeArgumentList(
                            TypeArgumentList(
                                SingletonSeparatedList<TypeSyntax>(
                                    IdentifierName(input.GenericTypeName))))))
            .WithParameterList(
                ParameterList(
                    SingletonSeparatedList<ParameterSyntax>(
                        Parameter(
                                Identifier("query"))
                            .WithType(
                                GenericName(
                                        Identifier("IQueryable"))
                                    .WithTypeArgumentList(
                                        TypeArgumentList(
                                            SingletonSeparatedList<TypeSyntax>(
                                                IdentifierName(input.GenericTypeName))))))))
            .WithBody(
                Block(
                    SingletonList<StatementSyntax>(
                        ThrowStatement(
                            ObjectCreationExpression(
                                    QualifiedName(
                                        IdentifierName("System"),
                                        IdentifierName("NotImplementedException")))
                                .WithArgumentList(
                                    ArgumentList())))));
    }
}