using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp.Syntax;

namespace CodeGenerator.InterfaceGenerator
{
    public class AccessRuleSetGenerateInfo : BaseGenerateInfo
    {
        public BaseListSyntax BaseInterface { get; set; }
        public SyntaxList<MemberDeclarationSyntax> MemberDeclarationSyntax { get; set; }
    }
}