using System.Collections.Generic;
using System.Linq;
using IOCContainerExtensions;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using static Microsoft.CodeAnalysis.CSharp.SyntaxFactory;

namespace CodeGenerator.InterfaceGenerator
{
    public class AccessRuleReWriteInfo : ReWriteInfoBase
    {
        public IEnumerable<ClassDeclarationSyntax> Classes { get; set; }
    }

    public class AccessRuleSetReWriter : IQuery<AccessRuleReWriteInfo, CompilationUnitSyntax>
    {
        private IQuery<ReWriteInfoBase, NamespaceUsingsInfo> getUsingsNamespaces { get; set; }

        public AccessRuleSetReWriter(IQuery<ReWriteInfoBase, NamespaceUsingsInfo> usingsNamespaces)
        {
            getUsingsNamespaces = usingsNamespaces;
        }

        public CompilationUnitSyntax Handle(AccessRuleReWriteInfo input)
        {
            var usingsAndNamespaces = getUsingsNamespaces.Handle(input);
            var namespacesAndClasses =
                usingsAndNamespaces.Namespaces.First().WithMembers(new SyntaxList<MemberDeclarationSyntax>(input.Classes));
            var result = CompilationUnit().AddUsings(usingsAndNamespaces.Usings.ToArray())
                .AddMembers(namespacesAndClasses);
            return result;
        }
    }
}