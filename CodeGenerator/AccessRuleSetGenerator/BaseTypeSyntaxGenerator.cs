using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.CSharp.Syntax;

namespace CodeGenerator.InterfaceGenerator
{
    public class BaseTypeSyntaxGenerator : IGeneratorByName<MemberDeclarationGenerateInfo,BaseTypeSyntax >
    {
        public BaseTypeSyntax Generate(MemberDeclarationGenerateInfo info)
        {
            return SyntaxFactory.SimpleBaseType(SyntaxFactory
                .GenericName(SyntaxFactory.Identifier(info.InterfaceName))
                .WithTypeArgumentList(SyntaxFactory.TypeArgumentList(
                    SyntaxFactory.SingletonSeparatedList<TypeSyntax>(
                        SyntaxFactory.IdentifierName(info.GenericTypeName)))));
        }
    }
}