using System.Collections.Generic;

namespace CodeGenerator.InterfaceGenerator
{
    public class InputAccessSetGenerateInfo
    {
        public string _dbSetSetName { get; set; }

        public string _dbSetTypeName { get; set; }

        public IEnumerable<string> _accessRoles { get; set; }
    }
}