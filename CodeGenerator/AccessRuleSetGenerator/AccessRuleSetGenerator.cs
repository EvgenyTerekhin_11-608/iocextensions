using System.Linq;
using System.Runtime.CompilerServices;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using static Microsoft.CodeAnalysis.CSharp.SyntaxFactory;

namespace CodeGenerator.InterfaceGenerator
{
    public class AccessRuleSetGenerator
    {
        private readonly IGeneratorByNames<MemberDeclarationGenerateInfo, MemberDeclarationSyntax> _generatorByNames;
        private readonly IGenerator<AccessRuleSetGenerateInfo, ClassDeclarationSyntax> _classGenerator;
        private readonly ISeparatedListGenerator<MemberDeclarationGenerateInfo, BaseTypeSyntax> _separatedListGenerator;

        public AccessRuleSetGenerator(
            ISeparatedListGenerator<MemberDeclarationGenerateInfo, BaseTypeSyntax> separatedListGenerator,
            IGeneratorByNames<MemberDeclarationGenerateInfo, MemberDeclarationSyntax> generatorByNames,
            IGenerator<AccessRuleSetGenerateInfo, ClassDeclarationSyntax> classGenerator)
        {
            _separatedListGenerator = separatedListGenerator;
            _generatorByNames = generatorByNames;
            _classGenerator = classGenerator;
        }

        public ClassDeclarationSyntax Generate(InputAccessSetGenerateInfo @in)
        {
            var memberDeclarationGenerateInfo = @in._accessRoles.Select(x =>
                new MemberDeclarationGenerateInfo
                {
                    InterfaceName = $"I{x}AccessRule", GenericTypeName = @in._dbSetTypeName
                }).ToList();
            var input = new AccessRuleSetGenerateInfo
            {
                Name = $"{@in._dbSetSetName}AccessSet",
                Modifiers = TokenList(Token(SyntaxKind.PublicKeyword)),
                BaseInterface = BaseList(_separatedListGenerator.Generate(memberDeclarationGenerateInfo)),
                MemberDeclarationSyntax = List(_generatorByNames.Generate(memberDeclarationGenerateInfo)),
            };
            return _classGenerator.Generate(input).NormalizeWhitespace();
        }
    }
}