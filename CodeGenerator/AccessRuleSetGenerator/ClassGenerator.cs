using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.CSharp.Syntax;

namespace CodeGenerator.InterfaceGenerator
{
    public class ClassGenerator : IGenerator<AccessRuleSetGenerateInfo, ClassDeclarationSyntax>
    {
        public ClassDeclarationSyntax Generate(AccessRuleSetGenerateInfo input) =>
            SyntaxFactory.ClassDeclaration(input.Name)
                .WithMembers(input.MemberDeclarationSyntax).WithBaseList(input.BaseInterface)
                .WithModifiers(input.Modifiers);
    }
}