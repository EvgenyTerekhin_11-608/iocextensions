using System.Collections.Generic;

namespace CodeGenerator
{
    public interface IGeneratorByNames<T,R>
    {
        IEnumerable<R> Generate(IEnumerable<T> names);
    }
    
}