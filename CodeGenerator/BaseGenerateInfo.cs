using Microsoft.CodeAnalysis;

namespace CodeGenerator
{
    public class BaseGenerateInfo
    {
        public SyntaxTokenList Modifiers { get; set; }
        public string Name { get; set; }
    }
}