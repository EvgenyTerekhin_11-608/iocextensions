﻿using System;
using System.Collections.Generic;
using IOCContainerExtensions;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Microsoft.EntityFrameworkCore;
using RoslynHelpers;
using static Microsoft.CodeAnalysis.CSharp.SyntaxFactory;

namespace CodeGenerator
{
    public class SeparatedSyntaxListGenerator<T, R> : ISeparatedListGenerator<T, R> where R : SyntaxNode
    {
        private readonly IGeneratorByNames<T, R> _generatorByName;

        public SeparatedSyntaxListGenerator(IGeneratorByNames<T, R> generatorByName)
        {
            _generatorByName = generatorByName;
        }

        public SeparatedSyntaxList<R> Generate(IEnumerable<T> names)
        {
            var result = _generatorByName.Generate(names);
            var syntaxNodeOrTokenList = result.JOIN<R, SyntaxToken, SyntaxNodeOrTokenList>((a, c) => a.Add(c),
                (a, c) => a.Add(c), Token(SyntaxKind.CommaToken));
            return SeparatedList<R>(syntaxNodeOrTokenList);
        }
    }
}