using System.Collections.Generic;
using System.Linq;

namespace CodeGenerator
{
    public class GeneratorByNames<T, R> : IGeneratorByNames<T, R>
    {
        private readonly IGeneratorByName<T, R> _generatorByName;

        public GeneratorByNames(IGeneratorByName<T, R> generatorByName)
        {
            _generatorByName = generatorByName;
        }

        public IEnumerable<R> Generate(IEnumerable<T> names) => names.Select(name => _generatorByName.Generate(name));
    }
}