namespace CodeGenerator
{
    public interface IGeneratorByName<R,T>
    {
        T Generate(R name);
    }
    
}