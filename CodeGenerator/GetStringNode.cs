using System.Collections.Generic;
using System.Linq;
using IOCContainerExtensions;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.CSharp.Syntax;

namespace CodeGenerator
{
    public interface IOpenGeneric2QuerySyntaxTree<T> : IQuery<SyntaxTree, IEnumerable<T>>
    {
    }

    public class GetStringNode<T> : IOpenGeneric2QuerySyntaxTree<T> where T : SyntaxNode
    {
        public IEnumerable<T> Handle(SyntaxTree tree) => tree.GetRoot().DescendantNodes().OfType<T>();
    }
}